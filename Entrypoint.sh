#!/bin/bash

set -e

# Выполнить команды
flask db upgrade
python seed.py
python -m unittest discover
gunicorn app:app -b 0.0.0.0:8000


# Сообщить об успешном завершении
echo "Команды выполнены успешно"
